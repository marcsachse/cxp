<?php
session_cache_limiter('nocache');
header('Expires: ' . gmdate('r', 0));
header('Content-type: application/json');

$Recipient = 'henle@henlekud.de'; // <-- Set your email here

if($Recipient) {

	$Name = filter_var($_POST['name'], FILTER_SANITIZE_STRING);
	$Vorname = filter_var($_POST['vorname'], FILTER_SANITIZE_STRING);
	$Firma = filter_var($_POST['firma'], FILTER_SANITIZE_STRING);
	$Strasse = filter_var($_POST['strasse'], FILTER_SANITIZE_STRING);
	$PLZ = filter_var($_POST['plz'], FILTER_SANITIZE_STRING);
	$Ort = filter_var($_POST['ort'], FILTER_SANITIZE_STRING);
	$Telefon = filter_var($_POST['tel'], FILTER_SANITIZE_STRING);
	$Email = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);
	$Subject = filter_var($_POST['subject'], FILTER_SANITIZE_STRING);
	$Message = filter_var($_POST['message'], FILTER_SANITIZE_STRING);
	if (isset($_POST['guests'])) {
		$Guests = filter_var($_POST['guests'], FILTER_SANITIZE_STRING);
	} else {
		$Guests = "";
	}
	if (isset($_POST['events'])) {
		$Events = filter_var($_POST['events'], FILTER_SANITIZE_STRING);
	} else {
		$Events = "";
	}
	if (isset($_POST['category'])) {
		$Category = filter_var($_POST['category'], FILTER_SANITIZE_STRING);
	} else {
		$Category = "";
	}

	$Email_body = "";
	$Email_body .=  "Name: " . $Name . "\n" .
					"Vorname: " . $Vorname . "\n" .
					"Firma: " . $Firma . "\n" .
					"Strasse: " . $Strasse . "\n" .
					"PLZ: " . $PLZ . "\n" .
					"Ort: " . $Ort . "\n" .
					"Tel: " . $Telefon . "\n" .
				   	"Email: " . $Email . "\n" .
				   	"Betreff: " . $Subject . "\n" .
				   	"Nachricht: " . $Message . "\n";

	$Email_headers = "";
	$Email_headers .= 'From: ' . $Name . ' <' . $Email . '>' . "\r\n".
					  "Reply-To: " .  $Email . "\r\n";

	$sent = mail($Recipient, $Subject, $Email_body, $Email_headers);

	if ($sent){
		$emailResult = array ('sent'=>'yes');
	} else{
		$emailResult = array ('sent'=>'no');
	}

	echo json_encode($emailResult);

} else {

	$emailResult = array ('sent'=>'no');
	echo json_encode($emailResult);

}
?>
